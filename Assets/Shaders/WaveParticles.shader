Shader "Custom/WaveParticles" {
Properties {
	_MainTex ("Particle TextureA", 2D) = "white" {}
	_TextureB ("Particle TextureB", 2D) = "white" {}
	_TextureC ("Particle TextureC", 2D) = "white" {}
	_TextureD ("Particle TextureD", 2D) = "white" {}
	_TextureE ("Particle TextureE", 2D) = "white" {}
	_TextureF ("Particle TextureF", 2D) = "white" {}
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
	Blend SrcAlpha OneMinusSrcAlpha
	ColorMask RGB
	Cull Off Lighting Off ZWrite Off
	
	SubShader {
		Pass {
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _TextureB;
			sampler2D _TextureC;
			sampler2D _TextureD;
			sampler2D _TextureE;
			sampler2D _TextureF;
			fixed4 _TintColor;
			
			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 texcoord : TEXCOORD0;
			};
			
			float4 _MainTex_ST;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				int selectedSamplerNo = _Time.y % 6;
				sampler2D selectedSampler;
				fixed4 col = tex2D(_MainTex, i.texcoord);
				if (selectedSamplerNo == 0)
				{
					col = tex2D(_MainTex, i.texcoord);
				}
				if (selectedSamplerNo == 1)
				{
					col = tex2D(_TextureB, i.texcoord);
				}
				if (selectedSamplerNo == 2)
				{
					col = tex2D(_TextureC, i.texcoord);
				}
				if (selectedSamplerNo == 3)
				{
					col = tex2D(_TextureD, i.texcoord);
				}
				if (selectedSamplerNo == 4)
				{
					col = tex2D(_TextureE, i.texcoord);
				}
				if (selectedSamplerNo == 5)
				{
					col = tex2D(_TextureF, i.texcoord);
				}
				//UNITY_APPLY_FOG_COLOR(i.fogCoord, col, fixed4(0,0,0,0)); // fog towards black due to our blend mode
				return col;
			}
			ENDCG 
		}
	}	
}
}
