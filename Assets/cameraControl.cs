﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraControl : MonoBehaviour {

	// Use this for initialization
	private Vector3 gameVeiw = new Vector3 (-1.5f, -0.5f, -8f);
	private Vector3 overVeiw = new Vector3 (0f, 0f, -12.5f);

	private Transform _transformTest;

	void Start ()
	{
		_transformTest = GetComponent<Transform>();
	}

	// Update is called once per frame
	void Update ()
	{

	}

	public void zoomToGame ()
	{
		transform.position = gameVeiw;
	}

	public void moveBack ()
	{
		transform.position = overVeiw;
	}
}
