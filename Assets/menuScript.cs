﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuScript : MonoBehaviour {

	// Use this for initialization
	private bool _haveSong;
	private bool _quitUp = false;

	public GameObject _MainMenu;
	public GameObject _SongMenu;
	public GameObject _ControlsMenu;
	public GameObject _CreditsMenu;
	public GameObject _QuitMenu;

	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{
		_QuitMenu.SetActive(_quitUp);
	}

//Start the Game!!!
	public void startButton ()
	{
		if (_haveSong)
		{
			Application.LoadLevel("MainScene");
		}
	}


//Song Menu Stuff
	public void songButton ()
	{
		_MainMenu.SetActive(false);
		_SongMenu.SetActive(true);
	}

	public void loadSong1 ()
	{
		_haveSong = true;
	}

	public void loadSongCustom ()
	{
		Debug.Log("Fancy Code Goes Here");
	}

	public void returnSongButton ()
	{
		_MainMenu.SetActive(true);
		_SongMenu.SetActive(false);
	}

//Controls Menu

	public void controlsButton ()
	{
		_MainMenu.SetActive(false);
		_ControlsMenu.SetActive(true);
	}

	public void returnControlsButton ()
	{
		_MainMenu.SetActive(true);
		_ControlsMenu.SetActive(false);
	}

//Credits Menu

	public void creditsButton ()
	{
		_MainMenu.SetActive(false);
		_CreditsMenu.SetActive(true);
	}

	public void returnCreditsButton ()
	{
		_MainMenu.SetActive(true);
		_CreditsMenu.SetActive(false);
	}

//Quit Menu Stuff
	public void quitButton ()
	{
		_quitUp = !_quitUp;
	}

	public void quitYes ()
	{
		Application.Quit();
	}

	public void quitNo ()
	{
		_quitUp = false;
	}
}
