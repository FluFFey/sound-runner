﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameController : MonoBehaviour {

    Animator animator;
    int flameSize;
    float flameDuration;
	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        flameSize = 0;
        flameDuration = 0.0f;
	}
	
	// Update is called once per frame
	void Update ()
    {
        flameDuration += Time.deltaTime;

        int flamesize = (int)(flameDuration /2.0f);
        animator.SetInteger("flameSize", flameSize);
    }
}
