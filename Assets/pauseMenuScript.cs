﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseMenuScript : MonoBehaviour {

	// Use this for initialization

	public GameObject _PauseMenu;
	private bool menuActive = false;

	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown("escape") || Input.GetKeyDown("p"))
		{
			toggleMenu();
		}
	}

	public void toggleMenu ()
	{
		menuActive = !menuActive;
		_PauseMenu.SetActive(menuActive);
	}

	public void returnToMainMenu()
	{
		Application.LoadLevel("menuScene");
	}
}
