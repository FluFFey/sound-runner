﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager> {

    public PlayerController playerBluePrint;
    public float dieMargin = 0.1f;

    private PlayerController _player_instance;

    public PlayerController playerInstance
    {
        get
        {
            return _player_instance;
        }
    }
    
	void Start () {
        _player_instance = Instantiate(playerBluePrint);

        Vector3 ppos = _player_instance.transform.position;
        _player_instance.transform.position = new Vector2(WaveData.Instance.charPos, ppos.y);
    }

    public void Die()
    {
        SceneManager.LoadScene("DeadScene");
    }
}
