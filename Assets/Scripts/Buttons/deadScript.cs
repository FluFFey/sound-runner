﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class deadScript : MonoBehaviour {

	// Use this for initialization

	public float _expireTime = 8f;
	public Text countText;

	public GameObject _highlightYes;
	public GameObject _highlightNah;

	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{
		if (_expireTime > 1)
		{
			_expireTime -= 1 * Time.fixedDeltaTime;
		} else {nah();}

		if (Input.GetKeyDown(KeyCode.Space))
		{
			yes();
		}

		countText.text = Mathf.FloorToInt(_expireTime).ToString();
	}

	public void yes ()
	{
		SceneManager.LoadScene("AlexScene");
	}

	public void nah ()
	{
		SceneManager.LoadScene("MainScene");
	}

	public void enterYes()
	{
		_highlightYes.SetActive(true);
	}
	public void exitYes()
	{
		_highlightYes.SetActive(false);
	}
	public void enterNah()
	{
		_highlightNah.SetActive(true);
	}
	public void exitNah()
	{
		_highlightNah.SetActive(false);
	}
}
