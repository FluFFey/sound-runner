﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class songButton : MonoBehaviour {

	// Use this for initialization

	public GameObject _highlight;
	public GameObject _speakers;

	private bool _songSelected;

	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{

	}

	public void phoneClick ()
	{
		_songSelected = !_songSelected;
		_speakers.SetActive(_songSelected);
	}

//Highlight Border
	public void onHover ()
	{
		_highlight.SetActive(true);
	}

	public void stopHover ()
	{
		_highlight.SetActive(false);
	}
}
