﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class playScript : MonoBehaviour
{
    // Use this for initialization

    public RectTransform trans;
    public Transform buttons;
    public GameObject _highlight;

    public GameObject _soundSystem;

    private bool to_play;
    public float toPlayScale;
    public float toPlayOffset = -2;
    public float transitionSpeed = 10f;

    private void Update()
    {
        if (to_play)
        {
            trans.localScale = Vector3.Lerp(trans.localScale, new Vector3(toPlayScale, toPlayScale, toPlayScale), Time.deltaTime);

            float x_mov = Mathf.Lerp(trans.transform.position.x, toPlayOffset, Time.deltaTime);
            trans.transform.position = new Vector3(x_mov, trans.transform.position.y, trans.transform.position.z);

            if (trans.localScale.x >= toPlayScale * 0.95f)
            {
                SceneManager.LoadScene("AlexScene");
            }
        }
    }

    public void startGame()
    {
        if (_soundSystem.activeSelf)
        {
            buttons.gameObject.SetActive(false);
            to_play = true;
        }
    }

    //Highlight Border
    public void onHover()
    {
        _highlight.SetActive(true);
        currentText._textNumber = 0;
    }

    public void stopHover()
    {
        _highlight.SetActive(false);
				currentText._textNumber = -1;
    }
}
