﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class youSure : MonoBehaviour {

	// Use this for initialization

	public GameObject _self;
	public GameObject _QuitButton;
	public GameObject _highlight;

	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{

	}

	public void onClick ()
	{
		Application.Quit();
	}

	public void stopHover ()
	{
		_self.SetActive(false);
		_QuitButton.SetActive(true);
		_highlight.SetActive(false);
	}
}
