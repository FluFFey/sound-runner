﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class quitScript : MonoBehaviour {

	// Use this for initialization

	public GameObject _highlight;

	public GameObject _self;
	public GameObject _youSure;

	void Start ()
	{

	}

	void onAwake ()
	{
		_highlight.SetActive(false);
	}

	// Update is called once per frame
	void Update ()
	{

	}

	public void onClick ()
	{
		_youSure.SetActive(true);
		_self.SetActive(false);

	}

	//Highlight Border
		public void onHover ()
		{
			_highlight.SetActive(true);
		}

		public void stopHover ()
		{
			_highlight.SetActive(false);
		}
}
