﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class currentText : MonoBehaviour {

	// Use this for initialization

/*
	public GameObject _playText;
	public GameObject _controlText;
	public GameObject _creditText;
	public GameObject _secretText;
*/

	public GameObject[] _currentText;

	public static int _textNumber = -1;

	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{
		for (var i = 0; i < _currentText.Length; i++)
		{
			if (i == _textNumber)
			{
				_currentText[i].SetActive(true);
			} else {_currentText[i].SetActive(false);}
		}
	}
}
