﻿using UnityEngine;

public class FloorCheck : MonoBehaviour
{
    public PlayerController player;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<WaveRenderer>())
        {
            player.isGrounded = true;
            Debug.Log("Now is grounded");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<WaveRenderer>())
        {
            player.isGrounded = false;
            Debug.Log("Now is not");

        }
    }
}
