﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class DieLine : MonoBehaviour
{
    public EnvironmentController environmentController;
    public bool falseBassTrueTreble;
    public float offset = 0.2f;

    private LineRenderer _rend;
    private float _line_pos;

    private float _time_to_thick;

    // Use this for initialization
    private void Start()
    {
        _rend = GetComponent<LineRenderer>();
        _rend.numPositions = 100;
        _rend.useWorldSpace = false;
    }

    private void Update()
    {
        WaveData wd = WaveData.Instance;
        float pos = falseBassTrueTreble ? wd.trebleToThunderValue : wd.bassToExplodeValue;
        float amp_fac = environmentController.amplitudeFactor;

        Vector3[] poses = new Vector3[100];

        if (_line_pos != pos * amp_fac)
        {
            for (int i = -50; i < WaveData.Instance.charPos + 5; i++)
            {
                _line_pos = pos * amp_fac + offset;
                poses[i + 50] = new Vector3(i, _line_pos);
            }

            _rend.SetPositions(poses);
        }

        WaveRenderer.CheckThickness(ref _time_to_thick, _rend);
    }
}