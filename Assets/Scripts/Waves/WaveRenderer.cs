﻿using System;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class WaveRenderer : MonoBehaviour
{
    public float amplitudeFactor = 1;
    public int channelToRender = 0;

    public WaveData.WaveProperties[] waves;

    private LineRenderer _rend;
    private EdgeCollider2D _coll;

    private float _time_to_change_thickness;

    private float _real_amplitude_factor = 1;

    private void Start()
    {
        _rend = GetComponent<LineRenderer>();
        _coll = GetComponent<EdgeCollider2D>();

        waves = WaveData.Instance.bufferChannels[channelToRender].channel;

        _rend.numPositions = waves.Length;



        _real_amplitude_factor = amplitudeFactor * transform.parent.GetComponent<WaveManager>().amplitudeFactor;

        _UpdateLine();
    }

    private void Update()
    {
        _UpdateLine();
    }

    private void _UpdateLine()
    {
        Vector3[] line_positions = new Vector3[waves.Length];
        Vector2[] line_positions_2 = new Vector2[waves.Length];

        for (int i = 0; i < waves.Length; i++)
        {
            line_positions[i] = new Vector2(i * WaveData.Instance.distanceBetweenSamples, waves[i].amplitude * _real_amplitude_factor);
            line_positions_2[i] = new Vector2(i * WaveData.Instance.distanceBetweenSamples, waves[i].amplitude * _real_amplitude_factor);
        }

        _rend.SetPositions(line_positions);


        _coll.points = line_positions_2;

        CheckThickness(ref _time_to_change_thickness, _rend);
 
    }

    public static void CheckThickness(ref float time_, LineRenderer rend_)
    {
        time_ += Time.deltaTime;

        if (time_ >= 0.1f)
        {
            AnimationCurve curve = new AnimationCurve();
            for (int i = 0; i < 100; i++)
            {
                float time_to_add = UnityEngine.Random.value;
                float value_to_add = UnityEngine.Random.value * 0.3f;
                value_to_add = Mathf.Clamp(value_to_add, 0.1f, 0.3f);


                Keyframe key_to_add = new Keyframe(time_to_add, value_to_add);
                key_to_add.inTangent = UnityEngine.Random.value;
                key_to_add.outTangent = UnityEngine.Random.value;

                curve.AddKey(key_to_add);
            }
            rend_.widthCurve = curve;

            time_ = 0f;
        }
    }



    //private void _UpdateLine()
    //{
    //    float distance = 0;
    //    List<Vector3> line_positions = new List<Vector3>();

    //    line_positions.Add(new Vector2(distance, waves[0].amplitude * _real_amplitude_factor));

    //    for (int i = 1; i < waves.Length; i++)
    //    {
    //        distance += WaveData.Instance.distanceBetweenSamples;

    //        if (waves[i] != waves[i - 1])
    //        {
    //            line_positions.Add(new Vector2(distance, waves[i - 1].amplitude * _real_amplitude_factor));

    //            distance += WaveData.Instance.distanceBetweenSamples;
    //            line_positions.Add(new Vector2(distance, waves[i].amplitude * _real_amplitude_factor));
    //        }
    //    }

    //    _rend.numPositions = line_positions.Count;
    //    _rend.SetPositions(line_positions.ToArray());
    //}
}