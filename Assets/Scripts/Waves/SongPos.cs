﻿using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class SongPos : MonoBehaviour
{
    private LineRenderer _line;

    // Use this for initialization
    private void Start()
    {
        _line = GetComponent<LineRenderer>();
        _line.numPositions = 2;

        WaveData wd = WaveData.Instance;
        float pos = wd.charPos;
        _line.SetPositions(new Vector3[2] { new Vector3(pos, -5), new Vector3(pos, 5) });
    }
}